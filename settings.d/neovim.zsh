if [[ ! -f /tmp/nvim_log ]]; then
    touch /tmp/nvim_log
fi
export NVIM_PYTHON_LOG_FILE="/tmp/nvim_log"
export NVIM_PYTHON_LOG_LEVEL="DEBUG"

