# Set git environment variables
export GIT_AUTHOR_NAME=$(git config user.name)
export GIT_AUTHOR_EMAIL=$(git config user.email)

# Expand path for .dotfiles
# Aliases used with git configuration
export PATH=~/.dotfiles/bin/git:$PATH

