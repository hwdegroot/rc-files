# salesforce sfdx dev tools
#https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_install_cli.htm#sfdx_setup_install_cli_linux
if [[ -d ~/sfdx/bin ]]; then
    export PATH=$PATH:~/sfdx/bin
fi
