# Java paths
export JAVA_HOME=$(update-alternatives --query javac | sed -n -e 's/Best: *\(.*\)\/bin\/javac/\1/p')

# ANDROID SDK
if [[ -d /usr/lib/android/sdk ]]; then
    export ANDROID_HOME=/usr/lib/android/sdk
    export ANDROID_SDK_ROOT=$ANDROID_HOME

    export PATH=$PATH:$ANDROID_SDK_ROOT/tools
    export PATH=$PATH:$ANDROID_SDK_ROOT/emulator
    export PATH=$PATH:$ANDROID_SDK_ROOT/platform-tools
fi
if [[ -d /opt/android-studio/bin ]]; then
    export PATH=$PATH:/opt/android-studio/bin
fi

