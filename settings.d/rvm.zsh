# RVM stuff
[[ -s $HOME/.rvm/scripts/rvm ]] && source $HOME/.rvm/scripts/rvm
if [[ -d $HOME/.rvm/rubies/default/gems ]]; then
  export GEM_HOME=$HOME/.rvm/rubies/default/gems
  export GEM_PATH=$GEM_HOME
  export PATH=$PATH:$HOME/.rvm/bin
  export PATH=$GEM_HOME/bin:$HOME/.rvm/rubies/default/bin:$PATH
fi
export PATH=$PATH:$HOME/.local/bin:$HOME/bin
export PATH=$PATH:/sbin:/usr/sbin:/usr/local/sbin:/bin:/usr/bin:/usr/local/bin:/var/lib/gems/2.5.0

