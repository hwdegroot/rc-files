if [[ -f /usr/local/bin/aws_completer ]]; then
    export PATH="/usr/local/bin/aws_completer:$PATH"
    autoload bashcompinit && bashcompinit
    complete -C '/usr/local/bin/aws_completer' aws
fi


# Added by Amplify CLI binary installer
if [[ -d "$HOME/.amplify/bin" ]]; then
    export PATH="$HOME/.amplify/bin:$PATH"
fi

