# PhpEnv
# https://github.com/phpenv/phpenv
if [[ -d ~/.phpenv ]]; then
    export PHPENV_ROOT="/home/rik/.phpenv"
    export PATH="${PHPENV_ROOT}/bin:${PATH}"
    eval "$(phpenv init -)"
fi

