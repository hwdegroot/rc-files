# Go binary paths
if [[ -d /usr/local/go/bin ]]; then
  export GOROOT=/usr/local/go
  export GOPATH=$HOME/Go
  export PATH=$PATH:$GOPATH/bin:$GOROOT/bin
  mkdir -p $GOPATH
fi

