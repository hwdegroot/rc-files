#!/bin/bash

# https://github.com/mihalea/i3expo
sudo su

cd ~/source

git clone https://github.com/mihalea/i3expo
cd i3expo

python3 setup.py install
pip install pyxdg i3ipc pygame

cd packages

gcc -shared -O3 -Wall -fPIC -Wl,-soname,prtscn -o prtscn.so prtscn.c -lX11
mkdir /usr/share/i3expo
cp defaultconfig /usr/share/i3expo/defaultconfig
cp prtscn.so /usr/share/i3expo/prtscn.so
