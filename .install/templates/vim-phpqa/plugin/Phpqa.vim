set termguicolors
set nocompatible              " be iMproved, required
set hidden
filetype off                  " required
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs

" set the runtime path to include Vundle and initialize
call plug#begin('~/.config/nvim/plugged')
" Python autocompletion
Plug 'davidhalter/jedi-vim'
" Vim airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Git plugin
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" docker
Plug 'docker/docker', {'rtp': '/contrib/syntax/vim/'}
" html
Plug 'docunext/closetag.vim'
" PHP stuf
Plug 'stephpy/vim-php-cs-fixer'
Plug 'joonty/vim-phpqa' " coverage visualize coverage
Plug 'joonty/vdebug'
Plug 'StanAngeloff/php.vim'

" Parenthesis matching
Plug 'luochen1990/rainbow'

" File browser
Plug 'scrooloose/nerdtree'
" JS
Plug 'pangloss/vim-javascript'
Plug 'posva/vim-vue'
Plug 'digitaltoad/vim-pug'
Plug 'gcorne/vim-sass-lint'

" Syntax
"Plug 'scrooloose/syntastic'

" Completion
Plug 'roxma/nvim-completion-manager'

" Indentation
Plug 'godlygeek/tabular'

Plug 'kopischke/vim-fetch'

"Funny shit
Plug 'AndrewRadev/discotheque.vim'

" Theming
" Nightowl theme
Plug 'haishanh/night-owl.vim'
" Gruvbox retro theme
Plug 'morhetz/gruvbox'
" Github scheme for merge conflicts
Plug 'endel/vim-github-colorscheme'
call plug#end()            " required
filetype plugin indent on    " required

"====================================================================="
"                      BASIC CONFIGURATION                            "
"====================================================================="
" Change end-of-line, space and tab characters.
set list
set listchars=eol:↲,trail:·,tab:»·,extends:»,precedes:«
hi NonText ctermfg=238 ctermbg=NONE guifg=#000000 guibg=NONE
hi SpecialKey ctermfg=130 ctermbg=NONE guifg=#af5f00 guibg=NONE
" Show trailing whitepace and spaces before a tab:
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd InsertLeave * redraw!
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWritePre * :%s/\s\+$//e

" redraw only when we need to.
set lazyredraw
" visual autocomplete for command menu
set wildmenu
" Enable syntax highlighting
syntax enable
" set linenumbers
set number
" Always indent spaces
set expandtab
" size of a hard tabstop
set tabstop=4
" size of an indent
set shiftwidth=4
" a combination of spaces and tabs are used to simulate tab stops at a width
" other than the (hard)tabstop
set softtabstop=4
" Enable mouse scrolling
set mouse=a
" Use system clipboard iso vim cut buffer
set clipboard=unnamed
" Search options
" highlight matches
set hlsearch
" Start searching as soon as characters are entered
set incsearch
" show the matching part of the pair for [] {} and ()
set showmatch
" Make the backspace key work
set backspace=2
" Folding
" enable folding
set foldenable
" 10 nested fold max
set foldnestmax=10
" open most folds by default
set foldlevelstart=10
" space open/closes folds
nnoremap <space> za
" fold based on indent level
set foldmethod=indent
" Insert date
nnoremap <C-t> "=strftime("%Y/%m/%d")<CR>p
nnoremap <C-T> "=strftime("%Y/%m/%d %H:%M:%S")<CR>
set modifiable

"================================================================="
"                    MAPPING CONFIGURATION
"================================================================="

" Use silent for previous and next buffer (switch tabs as you will)
" Remap <leader>
let mapleader="@"
map <Esc>[1;5A <C-Up>
map <Esc>[1;5B <C-Down>
map <Esc>[1;5C <C-Right>
map <Esc>[1;5D <C-Left>
map <Esc>[1;2A <S-Up>
map <Esc>[1;2B <S-Down>
map <Esc>[0;2C <S-Right>
map <Esc>[1;2D <S-Left>
map <Esc>[1;3A <Alt-Up>
map <Esc>[1;3B <Alt-Down>
map <Esc>[1;3C <Alt-Right>
map <Esc>[1;3D <Alt-Left>
nnoremap <C-Down> :m .+1<CR>==
nnoremap <C-Up> :m .-2<CR>==
inoremap <C-Down> <Esc>:m .+1<CR>==gi
inoremap <C-UP> <Esc>:m .-2<CR>==gi
vnoremap <C-Down> :m '>+1<CR>gv=gv
vnoremap <C-Up> :m '<-2<CR>gv=gv
noremap <silent> <C-Left> :bprev!<CR>
noremap <silent> <C-Right> :bnext!<CR>
nnoremap <C-l> :buffers!<CR>:buffer<Space>
noremap <silent> <C-d>b :diffg BA<CR>
noremap <silent> <C-d>r :diffg RE<CR>
noremap <silent> <C-d>l :diffg LO<CR>
map <C-c> :bd<CR>
" map du as undo pick command in vimdiff
nmap du :wincmd w<cr>:normal u<cr>:wincmd w<cr>
"================================================================="
"                    PLUGIN CONFIGURATION
"================================================================="
"""============== Rainbow parenthesis ========
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
"""===========================================

"""============= Syntax higligtinh for specific file types =======
" javascript/vue
autocmd BufEnter,BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css
" Php analyzer tool config (php-cs-fixer)
autocmd BufEnter,BufRead,BufNewFile .php_cs.{dist,cache} setlocal filetype=php
" Typescript
au BufRead,BufNewFile,BufEnter *.ts,*.tsx setf typescript
" Nginx files
au BufRead,BufNewFile,BufEnter /etc/nginx/*,/usr/local/nginx/conf/*,*.conf setf nginx
" enable all Python syntax highlighting features
let python_highlight_all = 1
" Jedi autocompletion stuff for vim
" https://github.com/davidhalter/jedi-vim
let g:jedi#use_splits_not_buffers = "right"
"""==============================================================

"""============= vim-airline ==========================
" Show the status line
" https://hackernoon.com/the-last-statusline-for-vim-a613048959b2
set laststatus=2
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = ''
let g:airline_right_sep = '«'
let g:airline_right_sep = ''
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline_theme='powerlineish'
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
let g:airline_solarized_bg='dark'
"""====================================================

"""========== Completion menu (mucomplete) ============
"let g:deoplete#enable_at_startup = 1
let g:mucomplete#enable_auto_at_startup = 1
let g:loaded_ruby_provider = 1
set completeopt+=longest,menuone,noinsert
set shortmess+=c   " Shut off completion messages
set belloff+=ctrlg " If Vim beeps during completion

" File browser
let g:netrw_liststyle = 3


"""========== joonty/phpqa =========
let g:phpqa_codecoverage_file = "clover.xml"
" Don't run messdetector on save (default = 1)
let g:phpqa_messdetector_autorun = 0
" Don't run codesniffer on save (default = 1)
let g:phpqa_codesniffer_autorun = 0
" Show code coverage on load (default = 0)
let g:phpqa_codecoverage_autorun = 1
" Do not run on write
let g:phpqa_open_loc = 0
let g:phpqa_run_on_write = 0
"""==================================

"""============ NERDTree =====
map <C-n> :NERDTreeToggle<CR>

let NERDTreeShowHidden=1
"""===========================

"""=========== Syntastic ================
let g:syntastic_sass_checkers=["sasslint"]
let g:syntastic_scss_checkers=["sasslint"]

let g:syntastic_javascript_checkers = ['eslint']
"autocmd FileType python compiler pylint
""""=====================================


"""============== Syntax highlighting + colorschemes ========
" Colorscheme
set background=dark
colorscheme gruvbox
syntax enable
" Highlight current line
set cursorline
let g:gruvbox_contrast_dark = 'hard'
let g:gruvbox_bold = 1
let g:gruvbox_italic = 1
let g:gruvbox_underline = 1
let g:gruvbox_undercurl = 1
"colorscheme night-owl

"" Fix some hightlighting and stuff after nightowl scheme
" Nightowltowl line highlightlighting....
highlight CursorLine      guibg=#1a2d3c               ctermbg=233 gui=NONE cterm=NONE
highlight GitGutterChange guibg=#3C3836 guifg=#bbbb00 ctermfg=3
highlight GitGutterAdd    guibg=#3C3836 guifg=#009900 ctermfg=2
highlight GitGutterDelete guibg=#3C3836 guifg=#ff2222 ctermfg=1
highlight CodeUncovered   guibg=#3C3836 guifg=Red
highlight CodeCovered     guibg=#3C3836 guifg=Green
"""==========================================================

"""===================== Git stuff ==========================
autocmd FileType gitcommit exec 'au VimEnter * startinsert'
autocmd Filetype gitcommit setlocal textwidth=72

" Nightowl stinks in diffing. So use gruvbox
if &diff
    "set lines=58
    "set columns=180
    "autocmd VimResized * exec "normal \<C-w>=
    "colorscheme github
    hi DiffAdd     ctermfg=233 ctermbg=194 guifg=#003300 guibg=#DDFFDD gui=none        cterm=none
    hi DiffChange              ctermbg=255               guibg=#ececec gui=none        cterm=none
    hi DiffText    ctermfg=233 ctermbg=189 guifg=#003300 guibg=#DDFFDD gui=none        cterm=none
    hi DiffDelete  ctermfg=252 ctermbg=224 guifg=#DDCCCC guibg=#FFDDDD gui=none        cterm=none
    hi CursorLine                          guifg=#000033 guibg=#DDDDFF gui=bold,italic cterm=none
    hi CursorColum ctermfg=15              guifg=#FFFFFF               gui=none        cterm=none
    hi Cursor      ctermfg=15              guifg=#FFFFFF               gui=none        cterm=none
    "highlight! link DiffText MatchParen
    "highlight DiffAdd    cterm=bold gui=none guibg=Yellow
    "highlight DiffDelete cterm=bold gui=none guibg=Yellow
    "highlight DiffChange cterm=bold gui=none guibg=Yellow
    "highlight DiffText   cterm=bold gui=none guibg=Yellow
endif
