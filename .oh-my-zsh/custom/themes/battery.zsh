#
# Battery
#

# ------------------------------------------------------------------------------
# Configuration
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# | BATTERY_SHOW | below threshold | above threshold | fully charged |
# |------------------------+-----------------+-----------------+---------------|
# | false                  | hidden          | hidden          | hidden        |
# | always                 | shown           | shown           | shown         |
# | true                   | shown           | hidden          | hidden        |
# | charged                | shown           | hidden          | shown         |
# | discharging            | shown           | shown           | shown         |
# ------------------------------------------------------------------------------

BATTERY_SHOW="${BATTERY_SHOW=true}"
BATTERY_PREFIX="${BATTERY_PREFIX=""}"
BATTERY_SUFFIX="${BATTERY_SUFFIX="$PROMPT_DEFAULT_SUFFIX"}"
BATTERY_SYMBOL_CHARGING="${BATTERY_SYMBOL_CHARGING="⇡"}"
BATTERY_SYMBOL_DISCHARGING="${BATTERY_SYMBOL_DISCHARGING="⇣"}"
BATTERY_SYMBOL_FULL="${BATTERY_SYMBOL_FULL="•"}"
BATTERY_THRESHOLD="${BATTERY_THRESHOLD=10}"

# ------------------------------------------------------------------------------
# Section
# ------------------------------------------------------------------------------

# Show section only if either of follow is true
# - Always show is true
# - battery percentage is below the given limit (default: 10%)
# - Battery is fully charged
# Escape % for display since it's a special character in zsh prompt expansion
battery() {
    [[ $BATTERY_SHOW == false ]] && return

    local battery_data battery_percent battery_status battery_color

    local battery=( $(command upower -e | grep battery) )

    # Return if no battery
    [[ -z $battery ]] && return

    battery_data=( )
    battery_percent=( )
    battery_status=( )

    for x in ${battery[@]}; do
        temp_data=$(upower -i $x)
        battery_data+=( $temp_data )
        battery_percent+=( "$(echo "$temp_data" | grep percentage | awk '{print $2}')" )
        battery_status+=( "$(echo "$temp_data" | grep state | awk '{print $2}')" )
        unset temp_data
    done

    local batt_status=()
    for i in {1..2}; do
        # Remove trailing % and symbols for comparison
        b_percent="$(echo ${battery_percent[$((i))]} | tr -d '%[,;]')"
        b_status=${battery_status[$((i))]}

        # Change color based on battery percentage
        if [[ $b_percent == 100 || $b_status =~ "(charged|full)" ]]; then
            battery_color="%{$fg[green]%}"
        elif [[ $b_percent -lt $BATTERY_THRESHOLD ]]; then
            battery_color="%{$fg[red]%}"
        else
            battery_color="%{$fg[yellow]%}"
        fi

        # Battery indicator based on current status of battery
        if [[ $BATTERY_SHOW == 'discharging' && $b_status == "charging" ]]; then
            return
        fi

        if [[ $b_status == "charging" ]];then
            battery_symbol="${BATTERY_SYMBOL_CHARGING}"
        elif [[ $b_status =~ "^[dD]ischarg.*" ]]; then
            battery_symbol="${BATTERY_SYMBOL_DISCHARGING}"
        else
            battery_symbol="${BATTERY_SYMBOL_FULL}"
        fi
        # Escape % for display since it's a special character in Zsh prompt expansion
        batt_status+="${battery_color}${BATTERY_PREFIX}${battery_symbol}${b_percent}%%${BATTERY_SUFFIX}%{$reset_color%}"
    done
    echo "${batt_status[@]} "
}
