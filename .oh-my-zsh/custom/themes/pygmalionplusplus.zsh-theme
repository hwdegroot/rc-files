# Yay! High voltage and arrows!
. `dirname $0`/battery.zsh
_virtualenv_prompt_info() {
    if [[ -n "$VIRTUAL_ENV" ]]; then
        ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX="%{$fg[blue]%}(%{$fg[red]%}"
        ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX="%{$fg[blue]%})%{$reset_color%}"
        echo "${ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX}`basename ${VIRTUAL_ENV}`${ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX} "
    else
        ZSH_THEME_VIRTUAL_ENV_PROMPT_PREFIX=""
        ZSH_THEME_VIRTUAL_ENV_PROMPT_SUFFIX=""
    fi
}

prompt_setup_pygmalion() {
  ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[green]%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
  ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[yellow]%}⚡%{$reset_color%}"
  ZSH_THEME_GIT_PROMPT_CLEAN=""


  base_prompt='%{$fg[magenta]%}%n%{$reset_color%}%{$fg[cyan]%}@%{$reset_color%}%{$fg[yellow]%}%m%{$reset_color%}%{$fg[red]%}:%{$reset_color%}%{$fg[blue]%}%0~%{$reset_color%}'
  post_prompt='%{$fg[yellow]%}\$%{$reset_color%} '

  base_prompt_nocolor=$(echo "$base_prompt" | perl -pe "s/%\{[^}]+\}//g")
  post_prompt_nocolor=$(echo "$post_prompt" | perl -pe "s/%\{[^}]+\}//g")

  precmd_functions+=(prompt_pygmalion_precmd)

}

prompt_time() {
    echo "🕑 %{$fg[105]%}%*%{$reset_color%}"
}

prompt_pygmalion_precmd(){
  ZSH_THEME_GIT_PROMPT_PREFIX="%{$reset_color%}%{$fg[green]%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
  ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[yellow]%}⚡%{$reset_color%}"
  ZSH_THEME_GIT_PROMPT_CLEAN=""

  if [[ $((`git status --porcelain 2>/dev/null | wc -l`)) -gt 0 ]]; then
    git_prompt_status=" ${ZSH_THEME_GIT_PROMPT_DIRTY}"
  else
    git_prompt_status="${ZSH_THEME_GIT_PROMPT_CLEAN}"
  fi

  local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
  #local gitinfo=$(git_prompt_info)
  local gitinfo=$(oh_my_git_info)
  local gitinfo_nocolor=$(echo "$gitinfo" | perl -pe "s/%\{[^}]+\}//g")
  local exp_nocolor="$(print -P \"$base_prompt_nocolor$gitinfo_nocolor$post_prompt_nocolor\")"
  local prompt_length=${#exp_nocolor}
  local virtualenv_prompt="$(_virtualenv_prompt_info)"

  local nl=""
  local battery_state="$(battery)"

  local current_time=$(prompt_time)

  #if [[ $prompt_length -gt 40 ]]; then
    nl=$'\n%{\r%}';
  #fi
  PROMPT="${nl}$(echo ${battery_state[@]})${gitinfo}${current_time}${nl}${ret_status}${virtualenv_prompt}${base_prompt}${git_prompt_status} ${post_prompt}"
}

prompt_setup_pygmalion


