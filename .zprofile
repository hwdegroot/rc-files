if [ -n "${ZSH_VERSION}" ]; then
  if [ -f "${HOME}/.zshrc" ]; then
    source "${HOME}/.zshrc"
  fi
fi

[[ -z $DISPLAY && $XDG_VTNR -eq 1 ]] && exec startx

