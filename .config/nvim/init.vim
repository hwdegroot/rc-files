set termguicolors
set nocompatible              " be iMproved, required
set hidden
filetype off                  " required
set laststatus=2   " Always show the statusline
set encoding=utf-8 " Necessary to show Unicode glyphs

" set the runtime path to include Vundle and initialize
call plug#begin('~/.config/nvim/plugged')
" ============ Layout of IDE ===================
" Vim airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" File browser
Plug 'scrooloose/nerdtree'
Plug 'scrooloose/nerdcommenter'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

" Git plugin
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

" Fuzzy file search
Plug 'ctrlpvim/ctrlp.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

" ============ Developer tools ============
"Plug 'stephpy/vim-php-cs-fixer'
Plug 'joonty/vim-phpqa' " coverage visualize coverage
Plug 'vim-vdebug/vdebug'
Plug 'dbakker/vim-projectroot'
" Indentation
Plug 'godlygeek/tabular'
" Emmet for vim
Plug 'mattn/emmet-vim'
"=========================================

" ============= Syntax helpers ============
" docker
Plug 'docker/docker', {'rtp': '/contrib/syntax/vim/'}
" PHP stuff
Plug 'StanAngeloff/php.vim'
" JS
Plug 'pangloss/vim-javascript'
Plug 'posva/vim-vue'
Plug 'leafgarland/typescript-vim'
Plug 'heavenshell/vim-jsdoc', {
  \ 'for': ['javascript', 'javascript.jsx','typescript'],
  \ 'do': 'make install'
\}
"Plug 'gcorne/vim-sass-lint'
Plug 'hail2u/vim-css3-syntax'
Plug 'cakebaker/scss-syntax.vim'
" Syntax
"Plug 'scrooloose/syntastic'
Plug 'cespare/vim-toml', { 'branch': 'main' }
" ========================================

Plug 'tpope/vim-abolish'

" Java plugin for auto completion
Plug 'artur-shaik/vim-javacomplete2'

"================= Theming ===============
" Parenthesis matching
Plug 'junegunn/rainbow_parentheses.vim'

" html
Plug 'docunext/closetag.vim'

"Funny shit
Plug 'AndrewRadev/discotheque.vim'
" Nightowl theme
"Plug 'haishanh/night-owl.vim'
" Gruvbox retro theme
"Plug 'morhetz/gruvbox'
" Github scheme for merge conflicts
"Plug 'endel/vim-github-colorscheme'
" Molokayo
Plug 'tomasr/molokai'
Plug 'fmoralesc/molokayo'
" Bettet icons
Plug 'ryanoasis/vim-devicons'
" Display colors inline
Plug 'joelstrouts/swatch.vim'

"" Vim pathfinder
"Plug 'AlphaMycelium/pathfinder.vim'
Plug 'roxma/python-support.nvim'
"=========================================
"

"========== Auto completion ==============
" Either use coc or ncm2
" Coc seems to be more lightweight
Plug 'neoclide/coc.nvim', {'branch': 'release'}


" Insane completion manager
"Plug 'ncm2/ncm2'
"Plug 'roxma/nvim-yarp'
" NOTE: you need to install completion sources to get completions. Check
" our wiki page for a list of sources: https://github.com/ncm2/ncm2/wiki
"Plug 'ncm2/ncm2-bufword'
"Plug 'ncm2/ncm2-path'
"Plug 'ncm2/ncm2-jedi'
"Plug 'phpactor/phpactor' ,  {'do': 'composer install', 'for': 'php'}
"Plug 'phpactor/ncm2-phpactor'
"Plug 'ncm2/ncm2-tern'
"Plug 'ncm2/ncm2-match-highlight'
"Plug 'ncm2/ncm2-ultisnips'
"Plug 'SirVer/ultisnips'
"Plug 'ncm2/ncm2-html-subscope'
"======================================
call plug#end()
filetype plugin indent on

"====================================================================="
"                      BASIC CONFIGURATION                            "
"====================================================================="
" Change end-of-line, space and tab characters.
set list
set listchars=eol:↲,trail:·,tab:»·,extends:»,precedes:«
hi NonText ctermfg=238 ctermbg=NONE guifg=#000000 guibg=NONE
hi SpecialKey ctermfg=130 ctermbg=NONE guifg=#af5f00 guibg=NONE
" Show trailing whitepace and spaces before a tab:
autocmd Syntax * syn match ExtraWhitespace /\s\+$\| \+\ze\t/
autocmd InsertLeave * redraw!
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
autocmd BufWritePre * :%s/\s\+$//e

" redraw only when we need to.
set lazyredraw
" visual autocomplete for command menu
set wildmenu
" Enable syntax highlighting
syntax enable
" set linenumbers
set number
" Always indent spaces
set expandtab
" size of a hard tabstop
set tabstop=4
" size of an indent
set shiftwidth=4
" a combination of spaces and tabs are used to simulate tab stops at a width
" other than the (hard)tabstop
set softtabstop=4
" Enable mouse scrolling
set mouse=a
" Use system clipboard iso vim cut buffer
set clipboard=unnamed
" Search options
" highlight matches
set hlsearch
" Start searching as soon as characters are entered
set incsearch
" show the matching part of the pair for [] {} and ()
set showmatch
" Make the backspace key work
set backspace=2
" Folding
" enable folding
set nofoldenable
" 10 nested fold max
set foldnestmax=10
" open most folds by default
set foldlevelstart=10
" space open/closes folds
nnoremap <space> za
" fold based on indent level
set foldmethod=indent
" Insert date
"nnoremap <C-t> "=strftime("%Y/%m/%d")<CR>p
"nnoremap <C-T> "=strftime("%Y/%m/%d %H:%M:%S")<CR>
set modifiable

"================================================================="
"                    MAPPING CONFIGURATION
"================================================================="

" Use silent for previous and next buffer (switch tabs as you will)
" Remap <leader>
map <Esc>[1;5A <C-Up>
map <Esc>[1;5B <C-Down>
map <Esc>[1;5C <C-Right>
map <Esc>[1;5D <C-Left>
map <Esc>[1;2A <S-Up>
map <Esc>[1;2B <S-Down>
map <Esc>[0;2C <S-Right>
map <Esc>[1;2D <S-Left>
nnoremap <C-Down> :m .+1<CR>==
nnoremap <C-Up> :m .-2<CR>==
inoremap <C-Down> <Esc>:m .+1<CR>==gi
inoremap <C-UP> <Esc>:m .-2<CR>==gi
vnoremap <C-Down> :m '>+1<CR>gv=gv
vnoremap <C-Up> :m '<-2<CR>gv=gv
noremap <silent> <C-Left> :bprev!<CR>
noremap <silent> <C-Right> :bnext!<CR>
nnoremap <C-l> :buffers!<CR>:buffer<Space>
noremap <silent> <C-d>b :diffg BA<CR>
noremap <silent> <C-d>r :diffg RE<CR>
noremap <silent> <C-d>l :diffg LO<CR>
map <C-c> :bd<CR>:wincmd l<CR>
nmap <Leader>v :vsplit<CR>
nmap <Leader>h :split<CR>
map <Leader>c ^I//<Esc>
"keep cursor in the middle all the time :)
"nnoremap <C-k> 3kzz
"nnoremap <C-j> 3jzz
"nnoremap k kzz
"nnoremap j jzz
"nnoremap p pzz
"nnoremap P Pzz
"nnoremap G Gzz
"nnoremap x xzz
"inoremap <ESC> <ESC>zz
"nnoremap <ENTER> <ENTER>zz
"inoremap <ENTER> <ENTER><ESC>zzi
nmap <silent> <Leader><Up> :wincmd k<CR>
nmap <silent> <Leader><Down> :wincmd j<CR>
nmap <silent> <Leader>h :wincmd h<CR>
nmap <silent> <C-S-Left> :wincmd h<CR>
nmap <silent> <Leader>l :wincmd l<CR>
nmap <silent> <C-S-Right> :wincmd l<CR>
map <silent> <Leader>s :so $MYVIMRC<CR>:wincmd l<CR>
map <silent> <Leader>$ :e $MYVIMRC<CR>
"nnoremap o o<ESC>zza
"nnoremap O O<ESC>zza
"nnoremap a a<ESC>zza
"nnoremap <C-f> <C-f>zz

" map du as undo pick command in vimdiff
nmap du :wincmd w<cr>:normal u<cr>:wincmd w<cr>
nmap <silent> <T>j 10j
nmap <silent> <T>k 10k
"================================================================="
"                    PLUGIN CONFIGURATION
"================================================================="
"""============= vim-airline ==========================
" Show the status line
" https://hackernoon.com/the-last-statusline-for-vim-a613048959b2
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.space = "\ua0"
set laststatus=2
let g:airline_powerline_fonts = 1

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = ''
let g:airline_right_sep = '«'
let g:airline_right_sep = ''
let g:airline_symbols.linenr = '¶'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

"        
" airline symbols
"let g:airline_left_sep = ''
"let g:airline_left_alt_sep = ''
"let g:airline_right_sep = ''
"let g:airline_right_alt_sep = ''
let g:airline_left_sep = ''
let g:airline_left_alt_sep = '' " ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = '' "''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline_theme='powerlineish'
"let g:airline_solarized_bg='dark'

"""============== Rainbow parenthesis ========
let g:rainbow_active = 1 "0 if you want to enable it later via :RainbowToggle
"""===========================================

"""============= Syntax higligtinh for specific file types =======
" sshconfig
autocmd BufEnter,BufRead,BufNewFile *.sshconfig setlocal filetype=sshconfig
" javascript/vue
autocmd BufEnter,BufRead,BufNewFile *.vue setlocal filetype=vue.html.javascript.css
" Php analyzer tool config (php-cs-fixer)
autocmd BufEnter,BufRead,BufNewFile .php_cs.{dist,cache} setlocal filetype=php
" Typescript
autocmd BufRead,BufNewFile,BufEnter *.[jt]sx? setlocal filetype=typescript.javascript
" Java syntax
autocmd BufRead,BufNewFile,BufEnter *.cls,*.java setlocal filetype=java
" Nginx files
autocmd BufRead,BufNewFile,BufEnter /etc/nginx/*,/usr/local/nginx/conf/*,*.conf setf nginx
" Toml configuration files
autocmd BufRead,BufNewFile,BufEnter *.toml setf toml
" enable all Python syntax highlighting features
let python_highlight_all = 1
" Jedi autocompletion stuff for vim
" https://github.com/davidhalter/jedi-vim
let g:jedi#use_splits_not_buffers = "right"
"""==============================================================

"""====================================================
" File browser
"""========== joonty/phpqa =========
let g:phpqa_codecoverage_file = "clover.xml"
" Don't run messdetector on save (default = 1)
let g:phpqa_messdetector_autorun = 0
" Don't run codesniffer on save (default = 1)
let g:phpqa_codesniffer_autorun = 0
" Show code coverage on load (default = 0)
let g:phpqa_codecoverage_autorun = 1
" Do not run on write
let g:phpqa_open_loc = 0
let g:phpqa_run_on_write = 0
"""==================================


"""=========== Syntastic ================
"let g:syntastic_sass_checkers=["sasslint"]
"let g:syntastic_scss_checkers=["sasslint"]
"
"let g:syntastic_javascript_checkers = ['eslint']
"autocmd FileType python compiler pylint
""""=====================================


"""============== Syntax highlighting + colorschemes ========
" Colorscheme
set background=dark
colorscheme molokayo
" https://github.com/joelstrouts/swatch.vimhttps://github.com/joelstrouts/swatch.vimhttps://github.com/joelstrouts/swatch.vim
" Activate
if exists('*Swatch_load') | call Swatch_load() | endif
syntax enable
" Highlight current line
set cursorline

"" Fix some hightlighting and stuff after nightowl scheme
" Nightowltowl line highlightlighting....
"highlight CursorLine      ctermbg=233                guibg=#1a2d3c gui=NONE cterm=NONE
"highlight GitGutterChange ctermfg=3    guibg=#3C3836 guifg=#bbbb00
"highlight GitGutterAdd    ctermfg=2    guibg=#3C3836 guifg=#009900
"highlight GitGutterDelete ctermfg=1    guibg=#3C3836 guifg=#ff2222
"highlight CodeUncovered   ctermfg=1    guibg=#3C3836 guifg=Red
"highlight CodeCovered     ctermfg=2    guibg=#3C3836 guifg=Green
"highlight CodeUncovered   ctermbg=NONE guibg=#3C3836 guifg=Red
"highlight CodeCovered     ctermbg=NONE guibg=#3C3836 guifg=Green
"highlight MessDetected    ctermbg=NONE guibg=#3C3836 guifg=Red
"highlight CodeSniffed     ctermbg=NONE guibg=#3C3836 guifg=Yellow
"""==========================================================
"Delete all Git conflict markers
"Creates the command :GremoveConflictMarkers
function! RemoveConflictMarkers() range
  echom a:firstline.'-'.a:lastline
  execute a:firstline.','.a:lastline . ' g/^<\{7}\|^|\{7}\|^=\{7}\|^>\{7}/d'
endfunction
"-range=% default is whole file
command! -range=% GDiffBoth <line1>,<line2>call RemoveConflictMarkers()


"""===================== Vim Vue ============================
let g:vue_pre_processors = 'detect_on_enter'
"""==========================================================

"""===================== Git stuff ==========================
autocmd FileType gitcommit exec 'autocmd VimEnter * startinsert'
autocmd Filetype gitcommit setlocal textwidth=72
autocmd VimEnter .git/* let s:git_action=1
autocmd VimEnter * if &diff | let s:git_action=1 | endif

"=========================== NCM2 =========================
" enable ncm2 for all buffers
"autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
"set completeopt=noinsert,menuone,noselect
"
" Press enter key to trigger snippet expansion
" The parameters are the same as `:help feedkeys()`
"inoremap <silent> <expr> <CR> ncm2_ultisnips#expand_or("\<CR>", 'n')
" CTRL-C doesn't trigger the InsertLeave autocmd . map to <ESC> instead.
"inoremap <c-c> <ESC>

" When the <Enter> key is pressed while the popup menu is visible, it only
" hides the menu. Use this mapping to close the menu and also start a new line
"inoremap <expr> <CR> (pumvisible() ? "\<c-y>" : "\<CR>")

" use <TAB> to select the popup menu:
"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" let g:UltiSnipsExpandTriggeri = "<Plug>(ultisnips_expand)"
"let g:UltiSnipsJumpForwardTrigger = "<c-j>"
"let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
"let g:UltiSnipsRemoveSelectModeMappings = 0

"let g:phpactorPhpBin = "/usr/bin/php"

"==========================================================
let g:airline#extensions#coc#enabled = 1

" Fuzzy file finder
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

"===========================================================
"COC config
"https://github.com/neoclide/coc.nvim
"==========================================================
" if hidden is not set, TextEdit might fail.
set hidden

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=2

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Revamp keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <C-d> <Plug>(coc-range-select)
xmap <silent> <C-d> <Plug>(coc-range-select)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

noremap <silent> <Leader>b :buffers!<CR>
"======================================================================
"End COC config
"======================================================================
"
"======================================================================
"NERD tree syntax
"======================================================================
"""============ NERDTree =====
map <C-n> :NERDTreeToggle<CR>

let NERDTreeShowHidden=1

"""===========================
let g:DevIconsDefaultFolderOpenSymbol = 'ﱮ'
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols = {} " needed
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['\.lock$'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['\.gitignore$'] = ''
let g:WebDevIconsUnicodeDecorateFileNodesPatternSymbols['\.xml$'] = ''
let g:DevIconsEnableFolderExtensionPatternMatching = 1
let g:NERDTreeFileExtensionHighlightFullName = 1
let g:NERDTreeExactMatchHighlightFullName = 1
let g:NERDTreePatternMatchHighlightFullName = 1

let g:NERDTreeHighlightFolders = 1 " enables folder icon highlighting using exact match
let g:NERDTreeHighlightFoldersFullName = 1 " highlights the folder name
" you can add these colors to your .vimrc to help customizing
let s:brown = "905532"
let s:aqua =  "3AFFDB"
let s:blue = "689FB6"
let s:darkBlue = "44788E"
let s:purple = "834F79"
let s:lightPurple = "834F79"
let s:red = "AE403F"
let s:beige = "F5C06F"
let s:yellow = "F09F17"
let s:orange = "D4843E"
let s:darkOrange = "F16529"
let s:pink = "CB6F6F"
let s:salmon = "EE6E73"
let s:green = "8FAA54"
let s:lightGreen = "31B53E"
let s:white = "FFFFFF"
let s:rspec_red = 'FE405F'
let s:git_orange = 'F54D27'
let s:gitlab_orange = 'E65328'

let g:NERDTreeExtensionHighlightColor = {} " this line is needed to avoid error
let g:NERDTreeExtensionHighlightColor['css'] = s:blue
let g:NERDTreeExtensionHighlightColor['xml'] = s:blue
let g:NERDTreeExtensionHighlightColor['yml'] = s:blue
let g:NERDTreeExtensionHighlightColor['yaml'] = s:blue
let g:NERDTreeExtensionHighlightColor['md'] = s:lightGreen
let g:NERDTreeExtensionHighlightColor['lock'] = s:red

let g:NERDTreeExactMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreeExactMatchHighlightColor['.gitignore'] = s:git_orange " sets the color for .gitignore files
let g:NERDTreeExactMatchHighlightColor['.gitlab-ci.yml'] = s:gitlab_orange " sets the color for .gitlab-ci.yml files
let g:NERDTreeExactMatchHighlightColor['.env'] = s:lightGreen

let g:NERDTreePatternMatchHighlightColor = {} " this line is needed to avoid error
let g:NERDTreePatternMatchHighlightColor['.*_spec\.rb$'] = s:rspec_red " sets the color for files ending with _spec.rb
"let g:NERDTreeDirArrowExpandable = '祈'
"let g:NERDTreeDirArrowCollapsible = '祉'
let g:NERDTreeDirArrowExpandable = ''
let g:NERDTreeDirArrowCollapsible = ''
let g:NERDTreeGlyphReadOnly = ''
hi NERDTreeRO guifg='Red'

augroup NERDTreeHideConcealSlashes
    autocmd!
    autocmd FileType nerdtree syntax match NERDTreeDirSlash #/[\s]*$# contained conceal containedin=ALL
    autocmd FileType nerdtree syntax match hideBracketsInNerdTree "\]" contained conceal containedin=ALL
    autocmd FileType nerdtree syntax match hideBracketsInNerdTree "\[" contained conceal containedin=ALL
    autocmd FileType nerdtree setlocal conceallevel=3
    autocmd FileType nerdtree setlocal concealcursor=nvic
augroup end

autocmd VimEnter * source $MYVIMRC
autocmd StdinReadPre * let s:std_in=1
"
"autocmd VimEnter * if !exists("s:std_in") && !exists("s:git_action") | NERDTree | :wincmd w | endif


if exists('g:loaded_webdevicons')
    call webdevicons#refresh()
endif
"======================================================================
"End of NERD tree syntax
"======================================================================
hi TabLineFill ctermfg=LightGreen ctermbg=DarkGreen
hi TabLine ctermfg=Blue ctermbg=Yellow
hi TabLineSel ctermfg=Red ctermbg=Yellow"


let g:python_support_python2_venv = 0
let g:python_support_python3_venv = 0
let g:python3_host_prog = '/usr/bin/python3'
let g:python_host_prog = '/usr/bin/python2'

"================================== Vdebug =============================
function! SetupDebug()
    let rootDir = finddir('.git/..', expand('%:p:h').';')
    let g:vdebug_options['ide_key']='PHPSTORM'
    let g:vdebug_options['port']=9000
    let g:vdebug_options['path_maps']={
        \ '/var/www/html/public': rootDir.'/resources/assets/static',
        \ '/var/www/html/app': rootDir.'/app',
        \ '/var/www/html/vendor': rootDir.'/vendor'
    \}
endfunction
nmap <leader>xd :call SetupDebug()<cr>


"======================= Git syntax ======================================
" syn clear gitcommitComment
" TODO Need to make git commentChar from config to be used dynamic
let g:gitCommentChar = system('git config core.commentchar')
syn match gitcommitComment '^>.*' oneline
if (&filetype == "gitcommit")
    syn match gitcommitFirstLine  '\%^[^>].*'  nextgroup=gitcommitBlank skipnl oneline
    "syn clear gitcommitSummary
    syn match gitcommitSummary    "^.\{0,72\}" contained containedin=gitcommitFirstLine nextgroup=gitcommitOverflow contains=@Spell
endif


" Moving back and forth
" Up half screen
" nunmap <silent> ^D
nnoremap <silent> <C-J> <C-D>
" Down half screen
"nunmap <silent> ^U
nnoremap <silent> <C-K> <C-U>
" down full screen
"nunmap <silent> ^F
nnoremap <silent> <C-S-J> <C-F>
" up full screen
"nunmap ^B
nnoremap <silent> <C-S-K> <C-B>

nnoremap <silent> <C-F> :FZF
" FZF search https://github.com/junegunn/fzf/blob/master/README-VIM.md#examples
" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" An action can be a reference to a function that processes selected lines
function! s:build_quickfix_list(lines)
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

let g:fzf_action = {
  \ 'ctrl-q': function('s:build_quickfix_list'),
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - Popup window (center of the screen)
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6 } }

" - Popup window (center of the current window)
let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.6, 'relative': v:true } }

" - Popup window (anchored to the bottom of the current window)
let g:fzf_layout = { 'window': { 'width': -1.9, 'height': 0.6, 'relative': v:true, 'yoffset': 1.0 } }

" - down / up / left / right
let g:fzf_layout = { 'down': '40%' }

" - Window using a Vim command
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
let g:fzf_layout = { 'window': '10new' }

" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of `--color` options
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

" Enable per-command history
" - History files will be stored in the specified directory
" - When set, CTRL-N and CTRL-P will be bound to 'next-history' and
"   'previous-history' instead of 'down' and 'up'.
let g:fzf_history_dir = '~/.local/share/fzf-history'
