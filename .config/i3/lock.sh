#/usr/bin/env bash

i3lock \
    --color=073b77 \
    --image=/home/rik/.config/i3/Pictures/nyan-cat.png  \
    --tiling \
    --nofork
